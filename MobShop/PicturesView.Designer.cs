﻿namespace MobShop
{
    partial class PicturesView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.image1 = new System.Windows.Forms.PictureBox();
            this.image2 = new System.Windows.Forms.PictureBox();
            this.image3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.image2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.image3)).BeginInit();
            this.SuspendLayout();
            // 
            // image1
            // 
            this.image1.Location = new System.Drawing.Point(12, 12);
            this.image1.Name = "image1";
            this.image1.Size = new System.Drawing.Size(224, 376);
            this.image1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.image1.TabIndex = 0;
            this.image1.TabStop = false;
            // 
            // image2
            // 
            this.image2.Location = new System.Drawing.Point(242, 12);
            this.image2.Name = "image2";
            this.image2.Size = new System.Drawing.Size(224, 376);
            this.image2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.image2.TabIndex = 1;
            this.image2.TabStop = false;
            // 
            // image3
            // 
            this.image3.Location = new System.Drawing.Point(472, 12);
            this.image3.Name = "image3";
            this.image3.Size = new System.Drawing.Size(224, 376);
            this.image3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.image3.TabIndex = 2;
            this.image3.TabStop = false;
            // 
            // PicturesView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 400);
            this.Controls.Add(this.image3);
            this.Controls.Add(this.image2);
            this.Controls.Add(this.image1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "PicturesView";
            this.Text = "PicturesView";
            ((System.ComponentModel.ISupportInitialize)(this.image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.image2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.image3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox image1;
        private System.Windows.Forms.PictureBox image2;
        private System.Windows.Forms.PictureBox image3;
    }
}