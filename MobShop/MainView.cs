﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MobShop
{
    public partial class MainView : Form
    {
        Readingsql FirstView = new Readingsql();
        List<Readingsql> all = null;
        public MainView()
        {
            all = FirstView.ReadAllObj();
            InitializeComponent();
            for(int i = 0; i < all.Count; i++)
            {
                Table.Rows.Add(all[i].mark, all[i].photo, all[i].sound, all[i].screen, all[i].battery, all[i].minutes, all[i].processor, all[i].power, all[i].cost, "Смотреть фото");
            }
        }

        private void MainView_Load(object sender, EventArgs e)
        {

        }

        private void Table_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                //Predicate<Readingsql> = 
                //узнаем модель телефона
                string mark = Table.Rows[e.RowIndex].Cells[0].Value.ToString();
                int i = all.FindIndex(x => x.mark == mark);
                //
                PicturesView a = new PicturesView(all[i]);
                a.ShowDialog();
            }
            catch (Exception i)
            {
                //MessageBox.Show("Зачем клацать на пустую кнопку?", "Не понял...", MessageBoxButtons.OK);
            }
        }

        private void Find_Click(object sender, EventArgs e)
        {
            List<string> Choo = new List<string>();
            Choo.Add(ChooBattery.Text == "" ? (" IS NOT NULL") : (">="+ChooBattery.Text));
            Choo.Add(ChooCamera.Text==""? (" IS NOT NULL") : (">=" + ChooCamera.Text));
            Choo.Add(ChooCost.Text== "" ? (" IS NOT NULL") : (">=" + ChooCost.Text));
            Choo.Add(ChooScreen.Text=="" ? (" IS NOT NULL") : (">=" + ChooScreen.Text));
            Choo.Add(ChooSound.Text== "" ? (" IS NOT NULL") : (">=" + ChooSound.Text));
            all = FirstView.ReadCustomObj(Choo);
            Table.Rows.Clear();
            for (int i = 0; i < all.Count; i++)
            {
                Table.Rows.Add(all[i].mark, all[i].photo, all[i].sound, all[i].screen, all[i].battery, all[i].minutes, all[i].processor, all[i].power, all[i].cost, "Смотреть фото");
            }
        }
    }
}
