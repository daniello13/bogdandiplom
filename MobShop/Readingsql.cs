﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace MobShop
{
    public class Readingsql
    {
        //SELECT * FROM telephones WHERE telephones.sound>9 AND telephones.mark IS NOT NULL
        // data for transfering
        public int id;
        public string mark;
        public int photo;
        public int sound;
        public int screen;
        public int battery;
        public int minutes;
        public string processor;
        public double power;
        public int cost;
        public string image1;
        public string image2;
        public string image3;
        public List<Readingsql> ReadAllObj()
        {
            List<Readingsql> all = new List<Readingsql>();
            string connStr = "server=localhost;user=root;database=bogdan_diplom;password=;";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "SELECT * FROM `telephones`";
            MySqlCommand command = new MySqlCommand(sql, conn);
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                // элементы массива [] - это значения столбцов из запроса SELECT
                //Console.WriteLine(reader[0].ToString() + " " + reader[1].ToString());
                Readingsql tmp = new Readingsql();
                tmp.id = Convert.ToInt32(reader[0].ToString());
                tmp.mark = reader[1].ToString();
                tmp.photo = Convert.ToInt32(reader[2].ToString());
                tmp.sound = Convert.ToInt32(reader[3].ToString());
                tmp.screen = Convert.ToInt32(reader[4].ToString());
                tmp.battery = Convert.ToInt32(reader[5].ToString());
                tmp.minutes = Convert.ToInt32(reader[6].ToString());
                tmp.processor = reader[7].ToString();
                tmp.power = Convert.ToDouble(reader[8].ToString());
                tmp.cost = Convert.ToInt32(reader[9].ToString());
                tmp.image1 = reader[10].ToString();
                tmp.image2 = reader[11].ToString();
                tmp.image3 = reader[12].ToString();
                all.Add(tmp);
                
            }
            return all;
        }
        public List<Readingsql> ReadCustomObj(List<string> a)
        {
            List<Readingsql> all = new List<Readingsql>();
            string connStr = "server=localhost;user=root;database=bogdan_diplom;password=;";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "SELECT * FROM `telephones` WHERE telephones.battery "+a[0]+ " AND telephones.photo "+a[1]+ " AND telephones.cost "+a[2]+ " AND telephones.screen "+a[3]+ " AND telephones.sound "+a[4];
            MySqlCommand command = new MySqlCommand(sql, conn);
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                // элементы массива [] - это значения столбцов из запроса SELECT
                //Console.WriteLine(reader[0].ToString() + " " + reader[1].ToString());
                Readingsql tmp = new Readingsql();
                tmp.id = Convert.ToInt32(reader[0].ToString());
                tmp.mark = reader[1].ToString();
                tmp.photo = Convert.ToInt32(reader[2].ToString());
                tmp.sound = Convert.ToInt32(reader[3].ToString());
                tmp.screen = Convert.ToInt32(reader[4].ToString());
                tmp.battery = Convert.ToInt32(reader[5].ToString());
                tmp.minutes = Convert.ToInt32(reader[6].ToString());
                tmp.processor = reader[7].ToString();
                tmp.power = Convert.ToDouble(reader[8].ToString());
                tmp.cost = Convert.ToInt32(reader[9].ToString());
                tmp.image1 = reader[10].ToString();
                tmp.image2 = reader[11].ToString();
                tmp.image3 = reader[12].ToString();
                all.Add(tmp);

            }
            return all;

        }
    }
}
