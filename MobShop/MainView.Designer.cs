﻿namespace MobShop
{
    partial class MainView
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Table = new System.Windows.Forms.DataGridView();
            this.Find = new System.Windows.Forms.Button();
            this.mark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.photo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sound = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.screen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.battery = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minutes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.processor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.image = new System.Windows.Forms.DataGridViewButtonColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ChooCamera = new System.Windows.Forms.TextBox();
            this.ChooSound = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ChooScreen = new System.Windows.Forms.TextBox();
            this.ChooBattery = new System.Windows.Forms.TextBox();
            this.ChooCost = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Table)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Table
            // 
            this.Table.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.mark,
            this.photo,
            this.sound,
            this.screen,
            this.battery,
            this.minutes,
            this.processor,
            this.power,
            this.cost,
            this.image});
            this.Table.Location = new System.Drawing.Point(12, 106);
            this.Table.Name = "Table";
            this.Table.Size = new System.Drawing.Size(836, 583);
            this.Table.TabIndex = 0;
            this.Table.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Table_CellContentClick);
            // 
            // Find
            // 
            this.Find.Location = new System.Drawing.Point(679, 13);
            this.Find.Name = "Find";
            this.Find.Size = new System.Drawing.Size(94, 87);
            this.Find.TabIndex = 1;
            this.Find.Text = "Найти";
            this.Find.UseVisualStyleBackColor = true;
            this.Find.Click += new System.EventHandler(this.Find_Click);
            // 
            // mark
            // 
            this.mark.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.mark.HeaderText = "Марка";
            this.mark.Name = "mark";
            this.mark.Width = 65;
            // 
            // photo
            // 
            this.photo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.photo.HeaderText = "Камера";
            this.photo.Name = "photo";
            this.photo.Width = 71;
            // 
            // sound
            // 
            this.sound.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.sound.HeaderText = "Звук";
            this.sound.Name = "sound";
            this.sound.Width = 56;
            // 
            // screen
            // 
            this.screen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.screen.HeaderText = "Экран";
            this.screen.Name = "screen";
            this.screen.Width = 63;
            // 
            // battery
            // 
            this.battery.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.battery.HeaderText = "Батарея";
            this.battery.Name = "battery";
            this.battery.Width = 74;
            // 
            // minutes
            // 
            this.minutes.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.minutes.HeaderText = "Время работы";
            this.minutes.Name = "minutes";
            this.minutes.Width = 96;
            // 
            // processor
            // 
            this.processor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.processor.HeaderText = "Процессор";
            this.processor.Name = "processor";
            this.processor.Width = 88;
            // 
            // power
            // 
            this.power.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.power.HeaderText = "Мощность";
            this.power.Name = "power";
            this.power.Width = 85;
            // 
            // cost
            // 
            this.cost.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.cost.HeaderText = "Цена";
            this.cost.Name = "cost";
            this.cost.Width = 58;
            // 
            // image
            // 
            this.image.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.image.HeaderText = "Фотографии";
            this.image.Name = "image";
            this.image.Width = 78;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ChooCost);
            this.groupBox1.Controls.Add(this.ChooBattery);
            this.groupBox1.Controls.Add(this.ChooScreen);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.ChooSound);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.ChooCamera);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(661, 87);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Требования";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(78, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Звук";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Камера";
            // 
            // ChooCamera
            // 
            this.ChooCamera.AccessibleDescription = "";
            this.ChooCamera.AccessibleName = "";
            this.ChooCamera.Location = new System.Drawing.Point(6, 47);
            this.ChooCamera.Name = "ChooCamera";
            this.ChooCamera.Size = new System.Drawing.Size(46, 20);
            this.ChooCamera.TabIndex = 5;
            this.ChooCamera.Tag = "апу";
            // 
            // ChooSound
            // 
            this.ChooSound.AccessibleDescription = "";
            this.ChooSound.AccessibleName = "";
            this.ChooSound.Location = new System.Drawing.Point(72, 47);
            this.ChooSound.Name = "ChooSound";
            this.ChooSound.Size = new System.Drawing.Size(46, 20);
            this.ChooSound.TabIndex = 8;
            this.ChooSound.Tag = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(144, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Экран";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(215, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Батарея";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(287, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Цена";
            // 
            // ChooScreen
            // 
            this.ChooScreen.AccessibleDescription = "";
            this.ChooScreen.AccessibleName = "";
            this.ChooScreen.Location = new System.Drawing.Point(142, 47);
            this.ChooScreen.Name = "ChooScreen";
            this.ChooScreen.Size = new System.Drawing.Size(46, 20);
            this.ChooScreen.TabIndex = 13;
            this.ChooScreen.Tag = "";
            // 
            // ChooBattery
            // 
            this.ChooBattery.AccessibleDescription = "";
            this.ChooBattery.AccessibleName = "";
            this.ChooBattery.Location = new System.Drawing.Point(218, 47);
            this.ChooBattery.Name = "ChooBattery";
            this.ChooBattery.Size = new System.Drawing.Size(46, 20);
            this.ChooBattery.TabIndex = 14;
            this.ChooBattery.Tag = "";
            // 
            // ChooCost
            // 
            this.ChooCost.AccessibleDescription = "";
            this.ChooCost.AccessibleName = "";
            this.ChooCost.Location = new System.Drawing.Point(278, 47);
            this.ChooCost.Name = "ChooCost";
            this.ChooCost.Size = new System.Drawing.Size(57, 20);
            this.ChooCost.TabIndex = 16;
            this.ChooCost.Tag = "";
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 701);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Find);
            this.Controls.Add(this.Table);
            this.Name = "MainView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds;
            this.Text = "MobShop";
            this.Load += new System.EventHandler(this.MainView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Table)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView Table;
        private System.Windows.Forms.Button Find;
        private System.Windows.Forms.DataGridViewTextBoxColumn mark;
        private System.Windows.Forms.DataGridViewTextBoxColumn photo;
        private System.Windows.Forms.DataGridViewTextBoxColumn sound;
        private System.Windows.Forms.DataGridViewTextBoxColumn screen;
        private System.Windows.Forms.DataGridViewTextBoxColumn battery;
        private System.Windows.Forms.DataGridViewTextBoxColumn minutes;
        private System.Windows.Forms.DataGridViewTextBoxColumn processor;
        private System.Windows.Forms.DataGridViewTextBoxColumn power;
        private System.Windows.Forms.DataGridViewTextBoxColumn cost;
        private System.Windows.Forms.DataGridViewButtonColumn image;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox ChooCost;
        private System.Windows.Forms.TextBox ChooBattery;
        private System.Windows.Forms.TextBox ChooScreen;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ChooSound;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ChooCamera;
    }
}

