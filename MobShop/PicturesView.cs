﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MobShop
{
    public partial class PicturesView : Form
    {
        public PicturesView(Readingsql a)
        {
            
            InitializeComponent();
            image1.Image = Image.FromFile(a.image1);
            image2.Image = Image.FromFile(a.image2);
            image3.Image = Image.FromFile(a.image3);
        }
    }
}
