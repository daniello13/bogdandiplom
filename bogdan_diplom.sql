-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Дек 12 2018 г., 05:34
-- Версия сервера: 10.1.31-MariaDB
-- Версия PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `bogdan_diplom`
--

-- --------------------------------------------------------

--
-- Структура таблицы `telephones`
--

CREATE TABLE `telephones` (
  `id` int(11) NOT NULL,
  `mark` varchar(100) CHARACTER SET utf16 NOT NULL,
  `photo` int(11) NOT NULL,
  `sound` int(11) NOT NULL,
  `screen` int(11) NOT NULL,
  `battery` int(11) NOT NULL,
  `minutes` int(11) NOT NULL,
  `processor` varchar(100) CHARACTER SET utf32 NOT NULL,
  `power` float NOT NULL,
  `cost` int(11) NOT NULL,
  `image1` varchar(500) NOT NULL,
  `image2` varchar(500) NOT NULL,
  `image3` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `telephones`
--

INSERT INTO `telephones` (`id`, `mark`, `photo`, `sound`, `screen`, `battery`, `minutes`, `processor`, `power`, `cost`, `image1`, `image2`, `image3`) VALUES
(1, 'Huawei P20 Pro', 100, 9, 10, 4000, 432, 'Kirin 970', 211.123, 24999, 'photos\\Huawei P20 Pro\\1.jpg', 'photos\\Huawei P20 Pro\\2.jpg', 'photos\\Huawei P20 Pro\\3.jpg'),
(2, 'Apple iPhone XS Max', 99, 10, 10, 3174, 308, 'A12 Bionic', 363.524, 38744, 'photos\\Apple iPhone XS Max\\1.jpg', 'photos\\Apple iPhone XS Max\\2.jpg', 'photos\\Apple iPhone XS Max\\3.jpg'),
(3, 'HTC U12+', 98, 10, 10, 3500, 272, 'Qualcomm Snapdragon 845', 275, 22700, 'photos\\HTC U12+\\1.jpg', 'photos\\HTC U12+\\2.jpg', 'photos\\HTC U12+\\3.jpg'),
(4, 'Samsung Galaxy Note 9', 97, 9, 10, 4000, 349, 'Samsung Exynos 9 9810', 235.456, 24999, 'photos\\Samsung Galaxy Note 9\\1.jpg', 'photos\\Samsung Galaxy Note 9\\2.jpg', 'photos\\Samsung Galaxy Note 9\\3.jpg'),
(5, 'Xiaomi Mi MIX 3', 96, 9, 9, 3200, 320, 'Qualcomm Snapdragon 845', 285.772, 18650, 'photos\\Xiaomi Mi MIX 3\\1.jpg', 'photos\\Xiaomi Mi MIX 3\\2.jpg', 'photos\\Xiaomi Mi MIX 3\\3.jpg'),
(6, 'Huawei P20', 95, 9, 9, 3400, 343, 'KIRIN 97', 213, 15952, 'photos\\Huawei P20\\1.jpg', 'photos\\Huawei P20\\2.jpg', 'photos\\Huawei P20\\3.jpg'),
(7, 'Samsung Galaxy S9 Plus', 94, 9, 10, 3500, 407, 'Samsung Exynos 9 9810', 242.789, 26150, 'photos\\Samsung Galaxy S9 Plus\\1.jpg', 'photos\\Samsung Galaxy S9 Plus\\2.jpg', 'photos\\Samsung Galaxy S9 Plus\\3.jpg'),
(8, 'Xiaomi Mi 8', 93, 7, 9, 3400, 340, 'Qualcomm Snapdragon 845', 271.164, 11541, 'photos\\Xiaomi Mi 8\\1.jpg', 'photos\\Xiaomi Mi 8\\2.jpg', 'photos\\Xiaomi Mi 8\\3.jpg'),
(9, 'Google Pixel 2', 92, 9, 9, 3250, 332, 'Qualcomm Snapdragon 835', 175, 16582, 'photos\\Google Pixel 2\\1.jpg', 'photos\\Google Pixel 2\\2.jpg', 'photos\\Google Pixel 2\\3.jpg'),
(10, 'Apple iPhone X', 91, 10, 9, 2760, 251, 'A11 Bionic', 245.149, 29974, 'photos\\Apple iPhone X\\1.jpg', 'photos\\Apple iPhone X\\2.jpg', 'photos\\Apple iPhone X\\3.jpg'),
(11, 'Huawei Mate 10 Pro', 90, 9, 8, 4000, 384, 'Kirin 970', 213.133, 15721, 'photos\\Huawei Mate 10 Pro\\1.jpg', 'photos\\Huawei Mate 10 Pro\\2.jpg', 'photos\\Huawei Mate 10 Pro\\3.jpg'),
(12, 'OnePlus 6Т', 89, 7, 9, 3300, 340, 'Qualcomm Snapdragon 845', 282.275, 18943, 'photos\\OnePlus 6T\\1.jpg', 'photos\\OnePlus 6T\\2.jpg', 'photos\\OnePlus 6T\\3.jpg'),
(13, 'Xiaomi Mi MIX 2S', 88, 7, 9, 3400, 353, 'Qualcomm Snapdragon 845', 277.178, 12561, 'photos\\Xiaomi Mi MIX 2S\\1.jpg', 'photos\\Xiaomi Mi MIX 2S\\2.jpg', 'photos\\Xiaomi Mi MIX 2S\\3.jpg'),
(14, 'OnePlus 6', 87, 7, 9, 3300, 284, 'Qualcomm Snapdragon 845', 282.275, 13544, 'photos\\OnePlus 6\\1.jpg', 'photos\\OnePlus 6\\2.jpg', 'photos\\OnePlus 6\\3.jpg'),
(15, 'Asus Zenfone Zoom S', 86, 6, 7, 5000, 398, 'Qualcomm Snapdragon 625', 62, 11394, 'photos\\Asus Zenfone Zoom S\\1.jpg', 'photos\\Asus Zenfone Zoom S\\2.jpg', 'photos\\Asus Zenfone Zoom S\\3.jpg'),
(16, 'Apple iPhone 8 Plus', 85, 8, 9, 2691, 306, 'A11 Bionic', 232.008, 23699, 'photos\\Apple iPhone 8 Plus\\1.jpg', 'photos\\Apple iPhone 8 Plus\\2.jpg', 'photos\\Apple iPhone 8 Plus\\3.jpg'),
(17, 'Samsung Galaxy Note 8', 84, 8, 9, 3300, 324, 'Samsung Exynos 8 Octa 8895', 199.868, 33674, 'photos\\Samsung Galaxy Note 8\\1.jpg', 'photos\\Samsung Galaxy Note 8\\2.jpg', 'photos\\Samsung Galaxy Note 8\\3.jpg'),
(18, 'Apple iPhone 8', 83, 8, 8, 1821, 261, 'A11 Bionic', 232.008, 21356, 'photos\\Apple iPhone 8\\1.jpg', 'photos\\Apple iPhone 8\\2.jpg', 'photos\\Apple iPhone 8\\3.jpg'),
(19, 'Asus ZenFone AR', 82, 8, 7, 3300, 271, 'Qualcomm Snapdragon 821', 165, 21812, 'photos\\Asus ZenFone AR\\1.jpg', 'photos\\Asus ZenFone AR\\2.jpg', 'photos\\Asus ZenFone AR\\3.jpg'),
(20, 'Smartisan Pro 2S', 81, 7, 7, 3600, 304, 'Qualcomm Snapdragon 660', 142, 9681, 'photos\\Smartisan Pro 2S\\1.jpg', 'photos\\Smartisan Pro 2S\\2.jpg', 'photos\\Smartisan Pro 2S\\3.jpg'),
(21, ' Asus Zenfone 5', 80, 9, 8, 3300, 293, 'Qualcomm Snapdragon 636', 136.645, 11116, 'photos\\Asus Zenfone 5\\1.jpg', 'photos\\Asus Zenfone 5\\2.jpg', 'photos\\Asus Zenfone 5\\3.jpg'),
(22, 'Google Pixel', 79, 8, 9, 2770, 231, 'Qualcomm Snapdragon 821', 145.384, 19576, 'photos\\Google Pixel\\1.jpg', 'photos\\Google Pixel\\2.jpg', 'photos\\Google Pixel\\3.jpg'),
(23, 'HTC U11', 78, 9, 9, 3000, 327, 'Qualcomm Snapdragon 835', 214, 15999, 'photos\\HTC U11\\1.jpg', 'photos\\HTC U11\\2.jpg', 'photos\\HTC U11\\3.jpg'),
(24, 'OnePlus 5Т', 77, 7, 8, 3300, 350, 'Qualcomm Snapdragon 835', 212.558, 13205, 'photos\\OnePlus 5?\\1.jpg', 'photos\\OnePlus 5?\\2.jpg', 'photos\\OnePlus 5?\\3.jpg'),
(25, 'Asus ZenFone 4 Max', 76, 5, 6, 5000, 371, 'Qualcomm Snapdragon 430', 43, 7825, 'photos\\Asus ZenFone 4 MAX\\1.jpg', 'photos\\Asus ZenFone 4 MAX\\2.jpg', 'photos\\Asus ZenFone 4 MAX\\3.jpg'),
(26, 'General Mobile GM9 Pro', 75, 6, 7, 3800, 323, 'Qualcomm Snapdragon 660', 143.845, 8647, 'photos\\General Mobile GM9 Pro\\1.jpg', 'photos\\General Mobile GM9 Pro\\2.jpg', 'photos\\General Mobile GM9 Pro\\3.jpg'),
(27, 'Vivo X20 Plus', 74, 8, 7, 3900, 340, 'Qualcomm Snapdragon 660', 142.418, 10775, 'photos\\Vivo X20 Plus\\1.jpg', 'photos\\Vivo X20 Plus\\2.jpg', 'photos\\Vivo X20 Plus\\3.jpg'),
(28, 'Xiaomi Mi Note 3', 73, 7, 7, 3500, 324, 'Qualcomm Snapdragon 660', 139.313, 7886, 'photos\\Xiaomi Mi Note 3\\1.jpg', 'photos\\Xiaomi Mi Note 3\\2.jpg', 'photos\\Xiaomi Mi Note 3\\3.jpg'),
(29, 'Moto Z', 72, 8, 7, 2600, 219, 'Qualcomm Snapdragon 820', 147.742, 10995, 'photos\\Moto Z\\1.jpg', 'photos\\Moto Z\\2.jpg', 'photos\\Moto Z\\3.jpg'),
(30, 'HTC 10', 71, 8, 7, 3000, 289, 'Qualcomm Snapdragon 820', 133.791, 7420, 'photos\\HTC 10\\1.jpg', 'photos\\HTC 10\\2.jpg', 'photos\\HTC 10\\3.jpg'),
(31, 'Samsung Galaxy S7 Edge', 70, 7, 8, 3000, 323, 'Samsung Exynos  Octa 8890', 158.313, 9189, 'photos\\Samsung Galaxy S7 Edge\\1.jpg', 'photos\\Samsung Galaxy S7 Edge\\2.jpg', 'photos\\Samsung Galaxy S7 Edge\\3.jpg'),
(32, 'Meizu 16 th', 69, 7, 9, 3010, 316, 'Qualcomm Snapdragon 845', 291.186, 12836, 'photos\\Meizu 16th\\1.jpg', 'photos\\Meizu 16th\\2.jpg', 'photos\\Meizu 16th\\3.jpg'),
(33, 'Apple iPhone 7 Plus', 68, 7, 8, 2900, 291, 'Apple A10 Fusion', 181, 20022, 'photos\\Apple iPhone 7 Plus\\1.jpg', 'photos\\Apple iPhone 7 Plus\\2.jpg', 'photos\\Apple iPhone 7 Plus\\3.jpg'),
(34, 'Xiaomi Pocophone F1', 67, 7, 7, 4000, 327, 'Qualcomm Snapdragon 845', 262.152, 8351, 'photos\\Xiaomi Pocophone F1\\1.jpg', 'photos\\Xiaomi Pocophone F1\\2.jpg', 'photos\\Xiaomi Pocophone F1\\3.jpg'),
(35, 'Apple iPhone 7 ', 66, 7, 8, 1960, 228, 'Apple A10 Fusion', 181, 14074, 'photos\\Apple iPhone 7\\1.jpg', 'photos\\Apple iPhone 7\\2.jpg', 'photos\\Apple iPhone 7\\3.jpg'),
(36, 'OnePlus 5', 65, 7, 7, 3300, 364, 'Qualcomm Snapdragon 835', 210.069, 13399, 'photos\\OnePlus 5\\1.jpg', 'photos\\OnePlus 5\\2.jpg', 'photos\\OnePlus 5\\3.jpg'),
(37, 'Meizu 16', 64, 6, 7, 3100, 289, 'Qualcomm Snapdragon 710', 164.726, 10483, 'photos\\Meizu 16\\1.jpg', 'photos\\Meizu 16\\2.jpg', 'photos\\Meizu 16\\3.jpg'),
(38, 'Crosscall Trekker-X4', 63, 5, 6, 4400, 365, 'Qualcomm Snapdragon 660', 132, 12862, 'photos\\Crosscall Trekker-X4\\1.jpg', 'photos\\Crosscall Trekker-X4\\2.jpg', 'photos\\Crosscall Trekker-X4\\3.jpg'),
(39, 'Nokia 7 Plus', 62, 6, 7, 3700, 292, 'Qualcomm Snapdragon 660', 141.701, 10499, 'photos\\Nokia 7 Plus\\1.jpg', 'photos\\Nokia 7 Plus\\2.jpg', 'photos\\Nokia 7 Plus\\3.jpg'),
(40, 'Nokia 8 Sirocco', 61, 6, 7, 3260, 273, 'Qualcomm Snapdragon 835', 207.434, 16143, 'photos\\Nokia 8 Sirocco\\1.jpg', 'photos\\Nokia 8 Sirocco\\2.jpg', 'photos\\Nokia 8 Sirocco\\3.jpg'),
(41, 'LG G7 ThinQ', 60, 10, 8, 3000, 289, 'Qualcomm Snapdragon 845', 250, 19764, 'photos\\LG G7 ThinQ\\1.jpg', 'photos\\LG G7 ThinQ\\2.jpg', 'photos\\LG G7 ThinQ\\3.jpg'),
(42, 'Sony Xperia XZ Premium', 59, 8, 7, 3230, 308, 'Qualcomm Snapdragon 835', 201.445, 13949, 'photos\\Sony Xperia XZ Premium\\1.jpg', 'photos\\Sony Xperia XZ Premium\\2.jpg', 'photos\\Sony Xperia XZ Premium\\3.jpg'),
(43, 'Xiaomi  MI 6', 58, 6, 6, 3350, 321, 'Qualcomm Snapdragon 835', 205.176, 10094, 'photos\\Xiaomi  MI 6\\1.jpg', 'photos\\Xiaomi  MI 6\\2.jpg', 'photos\\Xiaomi  MI 6\\3.jpg'),
(44, 'LG V30', 57, 9, 7, 3300, 296, 'Qualcomm Snapdragon 835', 160.289, 18997, 'photos\\LG V30\\1.jpg', 'photos\\LG V30\\2.jpg', 'photos\\LG V30\\3.jpg'),
(45, 'Motorola Moto Z2 Force', 56, 7, 5, 2730, 282, 'Qualcomm Snapdragon 835', 198.835, 13274, 'photos\\Motorola Moto Z2 Force\\1.jpg', 'photos\\Motorola Moto Z2 Force\\2.jpg', 'photos\\Motorola Moto Z2 Force\\3.jpg'),
(46, 'Huawei P Smart Plus', 55, 5, 6, 3340, 301, 'HiSilicon KIRIN 710', 138.583, 7301, 'photos\\Huawei P Smart Plus\\1.jpg', 'photos\\Huawei P Smart Plus\\2.jpg', 'photos\\Huawei P Smart Plus\\3.jpg'),
(47, 'Samsung Galaxy S6 Edge', 54, 6, 6, 2600, 237, 'Exynos 7 Octa 7420', 83.917, 9415, 'photos\\Samsung Galaxy S6 Edge\\1.jpg', 'photos\\Samsung Galaxy S6 Edge\\2.jpg', 'photos\\Samsung Galaxy S6 Edge\\3.jpg'),
(48, 'Xiaomi Redmi Note 5', 53, 5, 5, 4000, 327, 'Qualcomm Snapdragon 636', 116.663, 5536, 'photos\\Xiaomi Redmi Note 5\\1.jpg', 'photos\\Xiaomi Redmi Note 5\\2.jpg', 'photos\\Xiaomi Redmi Note 5\\3.jpg'),
(49, 'Archos Diamond Omega', 52, 6, 7, 3100, 264, 'Qualcomm Snapdragon 835', 170, 17500, 'photos\\Archos Diamond Omega\\1.jpg', 'photos\\Archos Diamond Omega\\2.jpg', 'photos\\Archos Diamond Omega\\3.jpg'),
(50, 'Samsung Galaxy A8 (2018)', 51, 6, 8, 3000, 251, 'Exynos 7 Octa 7885', 116.643, 12127, 'photos\\Samsung Galaxy A8 (2018)\\1.jpg', 'photos\\Samsung Galaxy A8 (2018)\\2.jpg', 'photos\\Samsung Galaxy A8 (2018)\\3.jpg'),
(51, 'Apple iPhone 6', 50, 7, 7, 1810, 208, 'Cyclone Apple A8', 79.1, 11454, 'photos\\Apple iPhone 6\\1.jpg', 'photos\\Apple iPhone 6\\2.jpg', 'photos\\Apple iPhone 6\\3.jpg'),
(52, 'Sony Xperia XA2 Ultra', 49, 7, 6, 3580, 329, 'Qualcomm Snapdragon 630', 75.525, 10430, 'photos\\Sony Xperia XA2 Ultra\\1.jpg', 'photos\\Sony Xperia XA2 Ultra\\2.jpg', 'photos\\Sony Xperia XA2 Ultra\\3.jpg'),
(53, 'Xiaomi Redmi S2', 48, 5, 6, 3080, 263, 'Qualcomm Snapdragon 625', 61, 4654, 'photos\\Xiaomi Redmi S2\\1.jpg', 'photos\\Xiaomi Redmi S2\\2.jpg', 'photos\\Xiaomi Redmi S2\\3.jpg'),
(54, 'Google Nexus 6P', 47, 7, 6, 3450, 313, 'Qualcomm Snapdragon 810', 83.906, 10684, 'photos\\Google Nexus 6P\\1.jpg', 'photos\\Google Nexus 6P\\2.jpg', 'photos\\Google Nexus 6P\\3.jpg'),
(55, 'Huawei Nova 2', 46, 7, 7, 2950, 287, 'HiSilicon KIRIN 659', 74.998, 8627, 'photos\\Huawei Nova 2\\1.jpg', 'photos\\Huawei Nova 2\\2.jpg', 'photos\\Huawei Nova 2\\3.jpg'),
(56, 'Meizu Pro 7 Plus', 45, 8, 7, 3500, 322, 'MediaTek Helio X30', 149.972, 7298, 'photos\\Meizu Pro 7 Plus\\1.jpg', 'photos\\Meizu Pro 7 Plus\\2.jpg', 'photos\\Meizu Pro 7 Plus\\3.jpg'),
(57, 'Xiaomi Mi A2 lite', 44, 5, 6, 4000, 362, 'Qualcomm Snapdragon 625', 76.5, 5071, 'photos\\Xiaomi Mi A2 lite\\1.jpg', 'photos\\Xiaomi Mi A2 lite\\2.jpg', 'photos\\Xiaomi Mi A2 lite\\3.jpg'),
(58, 'Xiaomi Mi A1', 43, 5, 6, 3080, 276, 'Qualcomm Snapdragon 625', 78.15, 4768, 'photos\\Xiaomi Mi A1\\1.jpg', 'photos\\Xiaomi Mi A1\\2.jpg', 'photos\\Xiaomi Mi A1\\3.jpg'),
(59, 'Huawei P Smart', 42, 4, 5, 3000, 323, 'HiSilicon KIRIN 659', 87.615, 7301, 'photos\\Huawei P Smart\\1.jpg', 'photos\\Huawei P Smart\\2.jpg', 'photos\\Huawei P Smart\\3.jpg'),
(60, 'Samsung Galaxy J5 Prime', 41, 5, 7, 2400, 271, 'Exynos 7 Quad 7570', 37.035, 6127, 'photos\\Samsung Galaxy J5 Prime\\1.jpg', 'photos\\Samsung Galaxy J5 Prime\\2.jpg', 'photos\\Samsung Galaxy J5 Prime\\3.jpg'),
(61, 'Samsung Galaxy S5', 40, 6, 6, 2800, 287, 'Qualcomm Snapdragon 801', 42.67, 4140, 'photos\\Samsung Galaxy S5\\1.jpg', 'photos\\Samsung Galaxy S5\\2.jpg', 'photos\\Samsung Galaxy S5\\3.jpg'),
(62, 'Xiaomi Redmi 5', 39, 4, 5, 3300, 433, 'Qualcomm Snapdragon 450', 58, 4289, 'photos\\Xiaomi Redmi 5\\1.jpg', 'photos\\Xiaomi Redmi 5\\2.jpg', 'photos\\Xiaomi Redmi 5\\3.jpg'),
(63, 'Motorola Moto G5S', 38, 5, 5, 3000, 312, 'Qualcomm Snapdragon 430', 43, 5720, 'photos\\Motorola Moto G5S\\1.jpg', 'photos\\Motorola Moto G5S\\2.jpg', 'photos\\Motorola Moto G5S\\3.jpg'),
(64, 'Apple iPhone 5s', 37, 6, 6, 1570, 196, 'Apple Cyclone ARMv8', 61.495, 5535, 'photos\\Apple iPhone 5s\\1.jpg', 'photos\\Apple iPhone 5s\\2.jpg', 'photos\\Apple iPhone 5s\\3.jpg'),
(65, 'Nokia 3', 36, 5, 4, 2650, 198, 'MediaTek MT6737', 26.5, 4749, 'photos\\Nokia 3\\1.jpg', 'photos\\Nokia 3\\2.jpg', 'photos\\Nokia 3\\3.jpg'),
(66, 'Meizu M5s', 35, 4, 4, 3000, 295, 'MediaTek MT6753', 45, 3461, 'photos\\Meizu M5s\\1.jpg', 'photos\\Meizu M5s\\2.jpg', 'photos\\Meizu M5s\\3.jpg'),
(67, 'Xiaomi Redmi 4', 34, 4, 4, 4100, 337, 'Qualcomm Snapdragon 430', 42.989, 4257, 'photos\\Xiaomi Redmi 4\\1.jpg', 'photos\\Xiaomi Redmi 4\\2.jpg', 'photos\\Xiaomi Redmi 4\\3.jpg'),
(68, 'Xiaomi Redmi 3S', 33, 4, 4, 4000, 325, 'Qualcomm Snapdragon 430', 37.647, 3523, 'photos\\Xiaomi Redmi 3S\\1.jpg', 'photos\\Xiaomi Redmi 3S\\2.jpg', 'photos\\Xiaomi Redmi 3S\\3.jpg'),
(69, 'Meizu M3s', 32, 5, 3, 3020, 283, 'MediaTek MT6750', 38.451, 5267, 'photos\\Meizu M3s\\1.jpg', 'photos\\Meizu M3s\\2.jpg', 'photos\\Meizu M3s\\3.jpg'),
(70, 'Apple iPhone 5c', 31, 6, 4, 1510, 198, 'Apple Swift ARMv7', 27.524, 4367, 'photos\\Apple iPhone 5c\\1.jpg', 'photos\\Apple iPhone 5c\\2.jpg', 'photos\\Apple iPhone 5c\\3.jpg');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `telephones`
--
ALTER TABLE `telephones`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `telephones`
--
ALTER TABLE `telephones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
